import React from "react";
import { Page, useNavigate, Box, Button } from "zmp-ui";
import { useRecoilValue } from "recoil";
import { userInfo } from "zmp-sdk";
import { userState } from "../../state";

import UserCard from "../../components/user-card";
import BottomNavigationRoute from "../../routes/bottom-navigation-page";

import { requestCameraPermission } from "zmp-sdk/apis";
import { showToast } from "zmp-sdk/apis";
import { Welcome } from "./welcome";

const HomePage: React.FunctionComponent = () => {
  const user = useRecoilValue<userInfo>(userState);

  const navigate = useNavigate();

  const requestCamera = async () => {
    try {
      const { userAllow, message } = await requestCameraPermission({});
      if (userAllow) {
      }
    } catch (error) {
      console.log(error);
    }
  };

  const openToast = async () => {
    try {
      const data = await showToast({
        message: "hello",
      });
      console.log(data);
    } catch (error) {
      // xử lý khi gọi api thất bại
      console.log(error);
    }
  };

  return (
    <Page className="page">
      <Welcome />
      <Box>
        <Button>Hello</Button>
        <Button>Hello</Button>
        <Button>Hello</Button>
        <Button>Hello</Button>
        <Button>Hello</Button>
        <Button>Hello</Button>
      </Box>
    </Page>
  );
};

export default HomePage;
