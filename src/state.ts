
import { atom, selector } from "recoil";
import { authorize, getSetting, getUserInfo, userInfo } from 'zmp-sdk';
import { Cart } from "./types/cart";

// export const userState = atom<userInfo>({
//   key: "user",
//   default: {
//     id: '031099',
//     name: 'Huy Tu',
//     avatar: 'Huy',
//   }
// })


export const authorizedState = selector({
  key: "authorized",
  get: async () => {
    const { authSetting } = await getSetting({});
    if (!authSetting["scope.userInfo"]) {
      await authorize({ scopes: [] });
    }
  },
});

export const userState = selector({
  key: "user",
  get: async ({ get }) => {
    get(authorizedState);
    const { userInfo } = await getUserInfo({ avatarType: "small" });
    return userInfo;
  },
});



export const cartState = atom<Cart>({
  key: "cart",
  default: [],
});
